package com.psybergate.git.tut;

import java.util.Date;

public class Customer {

	private String name;
	private String custNum;
	private Date dob;

	public Customer(String name, String custNum, Date dob) {
		super();
		this.name = name;
		this.custNum = custNum;
		this.dob = dob;
	}

	@Override
	public String toString() {
		return "Customer [name=" + name + ", custNum=" + custNum + ", dob=" + dob + "]";
	}

}